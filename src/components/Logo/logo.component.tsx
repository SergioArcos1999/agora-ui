import React, { Component } from 'react';
import LogoImage from "../../images/logos/main-logo.png"

class Logo extends Component {
    render() {
        return (
            <div className="logo-main">
                    <img src={LogoImage} alt="main-logo"/>
            </div>
        )
    }
}

export default Logo;