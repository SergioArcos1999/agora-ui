import { Component } from "react";

import "./home.css"
import FirstImage from "../../images/background-purple2.jpg";
import SecondImage from "../../images/background-battleship.jpg";
import ThirdImage from "../../images/background-courier-ship.jpg";
import LastImage from "../../images/background-stellaris-apocalypse.png";


type Props = {};

type State = {}

export default class Home extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <div className="h-1/3 w-full bg-gray-700" >

                <div className="grid grid-cols-1">
                    <div className="grid grid-cols-1 h-72 place-items-center bg-cover bg-center">
                        <img className="object-cover h-full w-full" src={FirstImage}/>
                        <div className="absolute top-21 px-6 py-4 text-center">
                            <h1 className="text-white">JOIN THE GALAXY</h1>
                            <span className="text-white">A vast galaxy full of resources ready to be explored</span>
                        </div>
                    </div>

                    <div className="grid grid-cols-1 h-72 place-items-center">
                        <img className="object-cover h-full w-full" src={SecondImage}/>
                        <div className="absolute top-25 px-6 py-4 text-center">
                            <h1 className="text-white">FIGHT WITH OR AGAINST OTHERS</h1>
                            <span className="text-white">Find your allies & fight in intense battles</span>
                        </div>
                    </div>
                    <div className="grid grid-cols-1 h-72 place-items-center">
                        <img className="object-cover h-full w-full" src={ThirdImage}/>
                        <div className="absolute top-90 px-6 py-4 text-center">
                            <h1 className="text-white">PROGRESS YOUR OWN WAY</h1>
                            <span className="text-white">Build your empire by fighting other players, mining plenty of resources or trading on the market</span>
                        </div>
                    </div>
                    <div className="grid grid-cols-1 h-full place-items-center">
                        <img className="object-cover h-full w-full" src={LastImage}/>
                        <div className="absolute top-90 px-6 py-4 text-center">
                            <h1 className="text-white">A CHANGING UNIVERSE</h1>
                            <span className="text-white">Decide the future of the universe</span>
                            <br/>
                            <span>Everything is in your hands</span>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}