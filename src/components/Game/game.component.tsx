import React, {Component} from "react";

import {Nav} from "react-bootstrap";
import {Link, Route, Switch} from "react-router-dom";
import Dashboard from "../Dashboard/dashboard.component";
import Map from "../Map/map.component";
import Production from "../Production/production.component";
import Hangar from "../Hangar/hangar.component";
import Defense from "../Defense/defense.component";
import Notifications from "../Notifications/notifications.component";
import Learn from "../Learn/learn.component";
import Profile from "../Profile/profile.component";
import AuthService from "../../services/auth.service";
import IUser from "../../types/user.type";
import EventBus from "../../common/event-bus";

import Logo from "../../images/logos/main-logo.png";

type Props = {};

type State = {
    currentUser: IUser | undefined
}

export default class Game extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.logOut = this.logOut.bind(this);

        this.state = {
            currentUser: undefined,
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();

        if (user) {
            this.setState({
                currentUser: user,
            });
        }

        EventBus.on("logout", this.logOut);
    }

    componentWillUnmount() {
        EventBus.remove("logout", this.logOut);
    }

    logOut() {
        AuthService.logout();
        window.location.reload();

        this.setState({
            currentUser: undefined,
        });
    }


    render() {
        const {currentUser} = this.state;

        return (
            <div className="h-full w-full flex bg-gray-800 pt-16">

                {/* TOP NAVBAR */}
                <Nav className="bg-gray-800 w-screen shadow h-16 fixed-top">
                    <div className="items-center w-screen relative flex">

                        {/* LOGO */}
                        <div className="w-1/12 flex-none">
                            <Nav.Link as={Link} to="/">
                                <div className="flex-shrink-0 flex items-center p-2">
                                    <img className="hidden lg:block h-8 w-8"
                                         src={Logo}
                                         alt="Mothership"/>
                                    <span className="ml-2 flex">Mothership</span>
                                </div>
                            </Nav.Link>
                        </div>

                        {/* MID MENU (RESOURCES) */}
                        <div className="w-11/12 h-max-16 mx-auto space-x-4 items-center justify-items-center font-extrabold">
                            <div className="w-8/12 h-max-10 flex mx-auto space-x-4 items-center justify-items-center">
                                <div
                                    className="flex-1 bg-gradient-to-br from-gray-700 via-violet-900 to-pink-900 rounded-l-md text-center p-1">
                                    <div
                                        className="flex-1 space-y-2 bg-gray-800 rounded-l-md text-center items-center justify-items-center">
                                        <Nav.Link as={Link} to="/production">
                                            <div className="text-sm">527.800</div>
                                        </Nav.Link>
                                    </div>
                                </div>

                                <div
                                    className="flex-1 bg-gradient-to-br from-gray-700 via-violet-900 to-pink-900 text-center p-1">
                                    <div
                                        className="flex-1 space-y-2 bg-gray-800 text-center items-center justify-items-center">
                                        <Nav.Link as={Link} to="/production">
                                            <div className="text-sm">527.800</div>
                                        </Nav.Link>
                                    </div>
                                </div>

                                <div
                                    className="flex-1 bg-gradient-to-br from-gray-700 via-violet-900 to-pink-900 rounded-r-md p-1">
                                    <div className="flex-1 space-y-2 bg-gray-800 rounded-r-md text-center">
                                        <Nav.Link as={Link} to="/production">
                                            <div className="text-sm">527.800</div>
                                        </Nav.Link>
                                    </div>
                                </div>

                            </div>
                        </div>

                        {/* RIGHT MENU */}
                        <div className="flex px-4 items-center">

                            {/* NOTIFICATIONS */}
                            <div className="rounded-md hover:bg-gray-700">
                                <Nav.Link as={Link} to="/notifications">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              stroke-width="2"
                                              d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"/>
                                    </svg>
                                </Nav.Link>
                            </div>

                            {/* LEARN */}
                            <div className="rounded-md hover:bg-gray-700">
                                <Nav.Link as={Link} to="/learn">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                         viewBox="0 0 24 24" stroke="currentColor">
                                        <path d="M12 14l9-5-9-5-9 5 9 5z"/>
                                        <path
                                            d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z"/>
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                              stroke-width="2"
                                              d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"/>
                                    </svg>
                                </Nav.Link>
                            </div>

                        </div>


                    </div>
                </Nav>


                {/* LEFT MENU (MAIN MENU) */}
                <Nav>
                    <aside className="flex flex-col items-center bg-gray-800 text-gray-700 shadow h-full w-full">

                        {/* NAVBAR */}
                        <ul className="px-2 py-3 space-y-2 w-full">

                            {/* DASHBOARD */}
                            <li className="h-12">
                                <Nav.Link className="h-11 flex justify-center items-center w-full hover:bg-gray-700 rounded-md focus:text-orange-500"
                                          as={Link} to="/">
                                    <div className="items-center flex w-full">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"/>
                                        </svg>
                                        <span className="ml-2 flex">Dashboard</span>
                                    </div>
                                </Nav.Link>
                            </li>

                            {/* MAP */}
                            <li className="h-12">
                                <Nav.Link className="h-11 flex justify-center items-center w-full rounded-md hover:bg-gray-700 focus:text-orange-500"
                                          as={Link} to="/map">
                                    <div className="items-center flex w-full">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                             viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"/>
                                        </svg>
                                        <span className="ml-2 flex">Map</span>
                                    </div>
                                </Nav.Link>
                            </li>

                            {/* PRODUCTION */}
                            <li className="h-12">
                                <Nav.Link className="h-11 flex justify-center items-center w-full rounded-md hover:bg-gray-700 focus:text-orange-500"
                                          as={Link} to="/production">
                                    <div className="items-center flex w-full">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                             viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"/>
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"/>
                                        </svg>
                                        <span className="ml-2 flex">Production</span>
                                    </div>
                                </Nav.Link>
                            </li>

                            {/* HANGAR */}
                            <li className="h-12">
                                <Nav.Link className="h-11 flex justify-center items-center w-full rounded-md hover:bg-gray-700 focus:text-orange-500"
                                          as={Link} to="/hangar">
                                    <div className="items-center flex w-full">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                             viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8"/>
                                        </svg>
                                        <span className="ml-2 flex">Hangar</span>
                                    </div>
                                </Nav.Link>
                            </li>

                            {/* Defense systems */}
                            <li className="h-12">
                                <Nav.Link className="h-11 flex justify-center items-center w-full rounded-md hover:bg-gray-700 focus:text-orange-500"
                                          as={Link} to="/defense">
                                    <div className="items-center flex w-full">
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                             viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M18.364 5.636l-3.536 3.536m0 5.656l3.536 3.536M9.172 9.172L5.636 5.636m3.536 9.192l-3.536 3.536M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-5 0a4 4 0 11-8 0 4 4 0 018 0z"/>
                                        </svg>
                                        <span className="ml-2 flex">Defense</span>
                                    </div>
                                </Nav.Link>
                            </li>
                        </ul>


                        {/* PROFILE & LOG OUT */}
                        <div className="mt-auto h-16 flex items-center w-full">

                            {/* PROFILE */}
                            <div className="flex flex justify-center items-center w-full h-full hover:bg-grey-700">
                                <li className="hover:bg-gray-500 h-full flex flex justify-center items-center">
                                    <Nav.Link as={Link} to="/profile">
                                        <div className="items-center flex w-full">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none"
                                                 viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                            </svg>
                                            <span className="ml-2 flex">Profile</span>
                                        </div>
                                    </Nav.Link>
                                </li>
                            </div>


                            {/* LOGOUT */}
                            <button
                                className="h-14 w-10 mx-auto flex justify-center items-center w-full hover:bg-violet-800">
                                <Nav.Link href="/" onClick={this.logOut}>
                                    <svg
                                        className="h-full"
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                        stroke="currentColor"
                                        stroke-width="2"
                                        stroke-linecap="round"
                                        stroke-linejoin="round">
                                        <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"/>
                                        <polyline points="16 17 21 12 16 7"/>
                                        <line x1="21" y1="12" x2="9" y2="12"/>
                                    </svg>
                                </Nav.Link>
                            </button>

                        </div>

                    </aside>
                </Nav>

                <div className="w-full h-full flex p-0 bg-gray-700">
                    <Switch>
                        <Route exact path="/" component={Dashboard}/>
                        <Route exact path="/map" component={Map}/>
                        <Route exact path="/production" component={Production}/>
                        <Route exact path="/hangar" component={Hangar}/>
                        <Route exact path="/defense" component={Defense}/>
                        <Route exact path="/notifications" component={Notifications}/>
                        <Route exact path="/learn" component={Learn}/>
                        <Route exact path="/profile" component={Profile}/>
                    </Switch>
                </div>

            </div>
        );
    }
}