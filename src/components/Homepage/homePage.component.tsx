import React, {Component} from "react";
import {Nav} from "react-bootstrap";
import {Link, Route, Switch} from "react-router-dom";
import Home from "../Home/home.component";
import News from "../News/news.component";
import Leaderboard from "../Leaderboard/leaderboard.component";
import Login from "../Login/login.component";
import Register from "../SignUp/register.component";
import AuthService from "../../services/auth.service";
import EventBus from "../../common/event-bus";
import IUser from "../../types/user.type";

import Logo from "../../images/logos/main-logo.png";

type Props = {};

type State = {
    currentUser: IUser | undefined
}

export default class HomePage extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.logOut = this.logOut.bind(this);

        this.state = {
            currentUser: undefined,
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();

        if (user) {
            this.setState({
                currentUser: user,
            });
        }

        EventBus.on("logout", this.logOut);
    }

    componentWillUnmount() {
        EventBus.remove("logout", this.logOut);
    }

    logOut() {
        AuthService.logout();
        window.location.reload();

        this.setState({
            currentUser: undefined,
        });
    }

    render() {
        const {currentUser} = this.state;

        return (
            <div className="w-full h-full bg-gray-800 pt-16">
                <Nav className="bg-gray-800 w-screen h-16 fixed-top">
                    <div className="w-screen px-2 sm:px-6 lg:px-8 relative flex">
                        <div className="flex">
                            {/* LOGO */}
                            <Nav.Link as={Link} to="/">
                                <div className="flex-shrink-0 flex items-center p-2">
                                    <img className="hidden lg:block h-8 w-auto"
                                         src={Logo}
                                         alt="Mothership"/>
                                    <span className="ml-2 flex">Mothership</span>
                                </div>
                            </Nav.Link>
                        </div>

                        <div className="flex items-center justify-between space-x-1">
                            <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                                {/* LEFT MENU */}
                                <div className="hidden sm:block sm:ml-6">
                                    <div className="flex space-x-4">

                                        {/* NEWS */}
                                        <Nav.Link as={Link} to="/news">
                                            <div>
                                                <button
                                                    className="text-gray-400 hover:bg-gray-700 hover:text-gray-100 focus:text-gray-100 rounded-lg text-sm focus:bg-gray-700 px-3 py-1.5 text-center">News</button>
                                            </div>
                                        </Nav.Link>

                                        {/* LEADERBOARD */}
                                        <Nav.Link as={Link} to="/leaderboard">
                                            <div>
                                                <button
                                                    className="text-gray-400 hover:bg-gray-700 hover:text-gray-100 rounded-lg text-sm focus:text-gray-100 focus:bg-gray-700 px-3 py-1.5 text-center">Leaderboard</button>
                                            </div>
                                        </Nav.Link>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="flex items-center justify-between space-x-1 absolute right-8 top-2.5">
                            <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                                {/* RIGHT MENU */}
                                <div className="hidden sm:block sm:ml-6">
                                    <div className="flex space-x-4">

                                        {/* SIGN IN */}
                                        <Nav.Link as={Link} to="/sign-in">
                                            <div>
                                                <button
                                                    className="text-gray-100 hover:bg-gray-700 ring-1 ring-gray-700 rounded-lg text-sm focus:bg-gray-700 px-3 py-1.5 text-center">Sign In</button>
                                            </div>
                                        </Nav.Link>

                                        {/* SIGN UP */}
                                        <Nav.Link as={Link} to="/sign-up">
                                            <div>
                                                <button
                                                    className="text-gray-100 bg-gradient-to-bl from-gray-700 via-violet-900 to-pink-900 hover:from-pink-900 hover:via-violet-900 hover:to-gray-700 focus:ring-2 focus:ring-violet-800 focus:from-pink-900 focus:via-violet-900 focus:to-gray-700 rounded-lg text-sm px-3 py-1.5 text-center">Sign Up</button>
                                            </div>
                                        </Nav.Link>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>
                </Nav>

                <div className="w-full h-full flex bg-gray-700">
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/news" component={News}/>
                        <Route path="/leaderboard" component={Leaderboard}/>
                        <Route path="/sign-in" component={Login}/>
                        <Route path="/sign-up" component={Register}/>
                    </Switch>
                </div>




            </div>
        );
    }
}