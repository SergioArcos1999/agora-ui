import {Component} from "react";
import {Link, RouteComponentProps} from "react-router-dom";
import {Formik, Field, Form, ErrorMessage} from "formik";
import * as Yup from "yup";

import Image from "../../images/background-ships.jpg";

import AuthService from "../../services/auth.service";

interface RouterProps {
    history: string;
}

type Props = RouteComponentProps<RouterProps>;

type State = {
    username: string,
    password: string,
    loading: boolean,
    message: string
};

export default class Login extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);

        this.state = {
            username: "",
            password: "",
            loading: false,
            message: ""
        };
    }

    validationSchema() {
        return Yup.object().shape({
            username: Yup.string().required("This field is required!"),
            password: Yup.string().required("This field is required!"),
        });
    }

    handleLogin(formValue: { username: string; password: string }) {
        const {username, password} = formValue;

        this.setState({
            message: "",
            loading: true
        });


        AuthService.login(username, password).then(
            () => {
                this.props.history.push("/management");
                window.location.reload();
            },
            error => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                this.setState({
                    loading: false,
                    message: resMessage
                });
            }
        );
    }

    render() {
        const {loading, message} = this.state;

        const initialValues = {
            username: "",
            password: "",
        };

        return (
            <div className="h-full w-full flex place-content-stretch">

                <div className="w-100 mx-auto bg-gray-800 shadow-md max-w-sm p-4">

                    <Formik
                        className="space-y-6"
                        initialValues={initialValues}
                        validationSchema={this.validationSchema}
                        onSubmit={this.handleLogin}
                    >
                        <Form className="space-y-3">
                            {/* HEADER */}
                            <span className="text-xl font-medium">Sign in</span>

                            {/* USERNAME */}
                            <div className="form-group">
                                <label htmlFor="username"
                                       className="text-sm font-medium text-gray-100 block mb-2">Username</label>
                                <Field name="username" type="text" className="form-control" placeholder="Username"/>
                                <ErrorMessage name="username" component="div" className="alert alert-danger"/>
                            </div>

                            {/* PASSWORD */}
                            <div className="form-group">
                                <label htmlFor="password"
                                       className="text-sm font-medium text-gray-100 block mb-2">Password</label>
                                <Field name="password" type="password" placeholder="••••••••••"
                                       className="form-control"/>
                                <ErrorMessage name="password" component="div" className="alert alert-danger"/>
                            </div>

                            {/* PASSWORD RECOVERY */}
                            <div className="flex items-center justify-center">
                                <a href="/account-recovery"
                                   className="mx-auto text-sm text-blue-400 hover:underline ml-auto">Lost Password?</a>
                            </div>

                            {/* SUBMIT */}
                            <button type="submit"
                                    className="w-full text-grey-100 bg-gradient-to-bl from-gray-700 via-violet-900 to-pink-900 hover:from-pink-900 hover:via-violet-900 hover:to-gray-700 focus:ring-1 focus:ring-violet-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center">
                                Login to your account
                            </button>

                            {/* CREATE ACCOUNT */}
                            <div className="text-sm font-medium text-gray-100 py-3">
                                Not a member? <a href="/sign-up" className="text-blue-400 hover:underline">Create account</a>
                            </div>
                        </Form>
                    </Formik>
                </div>

                {/* BACKGROUND */}
                <div className="w-100 mx-auto shadow-md">
                    <img className="object-cover h-full w-full" src={Image}/>
                </div>
            </div>
        );
    }
}