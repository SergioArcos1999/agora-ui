import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../../services/auth.service";
import IUser from "../../types/user.type";

type Props = {};

type State = {
    redirect: string | null,
    userReady: boolean,
    currentUser: IUser & { accessToken: string }
}
export default class Profile extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            redirect: null,
            userReady: false,
            currentUser: { accessToken: "" }
        };
    }

    componentDidMount() {
        const currentUser = AuthService.getCurrentUser();

        if (!currentUser) this.setState({ redirect: "/home" });
        this.setState({ currentUser: currentUser, userReady: true })
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        const { currentUser } = this.state;

        return (
            <div className="container mx-auto p-4">
                {(this.state.userReady) ?
                    <div className="content-start flex space-x-4 justify-items-stretch">

                        <div className="container mx-auto p-0">
                            <div className="flex space-x-4 p-8 justify-items-stretch">

                                <div className="flex-1 bg-gray-500 rounded-l-lg text-center">
                                    <div>LEFT</div>
                                </div>

                                <div className="flex-1 bg-gray-500 text-center">
                                    <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-16 w-16" fill="none"
                                             viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                  d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                        </svg>
                                    </div>
                                    <div>
                                        <header className="jumbotron">
                                            <h3>
                                                <strong>{currentUser.username}</strong> Profile
                                            </h3>
                                        </header>
                                    </div>
                                    <p>
                                        <strong>Token:</strong>{" "}
                                        {currentUser.accessToken.substring(0, 20)} ...{" "}
                                        {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
                                    </p>
                                    <p>
                                        <strong>Id:</strong>{" "}
                                        {currentUser.id}
                                    </p>
                                    <p>
                                        <strong>Email:</strong>{" "}
                                        {currentUser.email}
                                    </p>
                                    <strong>Authorities:</strong>
                                    <ul>
                                        {currentUser.roles &&
                                        currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
                                    </ul>
                                </div>
                                <div className="flex-1 bg-gray-500 rounded-r-lg text-center">
                                    <div>RIGHT</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    : null}
            </div>
        );
    }
}