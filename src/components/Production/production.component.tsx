import {Component} from "react";
import NuclearReactor from "../../images/buildings/nuclear-reactor.jpg";

type Props = {};

type State = {
    totalPlayers: string;
    onlinePlayers: string;
    serverStatus: string;
}

export default class Home extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            totalPlayers: "",
            onlinePlayers: "",
            serverStatus: ""
        };
    }

    componentDidMount() {

    }

    render() {
        return (
            <div className="h-full w-full px-5 py-4 space-y-4">

                {/* RESOURCES */}
                <div className="rounded-md shadow bg-gray-800 flex space-x-4 px-5 py-4">

                    {/* RESOURCE 1 */}
                    <div className="flex-1 shadow bg-gradient-to-bl from-gray-700 via-violet-900 to-pink-900 rounded-l-md text-center p-2">
                        <div className="flex-1 space-y-2 bg-gray-800 rounded-l-md text-center items-center justify-items-center p-4">
                            <div className="flex w-full space-x-2 items-center justify-items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" className="flex h-6 w-6" fill="none"
                                     viewBox="0 0 24 24" stroke="purple">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4"/>
                                </svg>
                                <span className="flex">Vortex</span>
                            </div>
                            <div>527.800</div>
                            <div className="rounded-full py-1 ring ring-1 ring-green-500 text-green-500">+ 10.532/h</div>
                        </div>
                    </div>

                    <div className="flex-1 shadow bg-gradient-to-b from-gray-700 via-violet-900 to-pink-900 text-center p-2">
                        <div className="flex-1 space-y-2 bg-gray-800 text-center items-center justify-items-center p-4">
                            <div>Resource 2</div>
                            <div>527.800</div>
                            <div className="rounded-full py-1 ring ring-1 ring-green-500 text-green-500">+ 10.532/h</div>
                        </div>
                    </div>

                    <div className="flex-1 shadow bg-gradient-to-br from-gray-700 via-violet-900 to-pink-900 rounded-r-md p-2">
                        <div className="flex-1 space-y-2 bg-gray-800 rounded-r-md text-center p-4">
                            <div>Resource 3</div>
                            <div>527.800</div>
                            <div className="rounded-full py-1 ring ring-1 ring-green-500 text-green-500">+ 10.532/h</div>
                        </div>
                    </div>
                </div>

                {/* PRODUCTION */}
                <div className="rounded-md shadow bg-gray-800 space-x-4 px-5 py-4">

                    <div className="text-3xl font-extrabold px-3">
                        {/* TODO: Meterle un gradient al texto, por algún motivo no consigo sacarlo  https://tailwindcss.com/docs/background-clip#cropping-to-text*/}
                        <span className="bg-gradient-to-r from-violet-900 to-violet-700 bg-clip-text text-transparent">PRODUCTION</span>
                    </div>

                    <div className="flex space-x-4 py-3">

                        {/* BUILDING 1 */}
                        <div className="flex-1 h-72 shadow bg-gradient-to-br from-gray-700 via-violet-900 to-pink-900 rounded-l-md text-center p-2">
                            <button className="flex-none h-full w-full hover:opacity-25 transition duration-350 ease-in-out">
                                <img className="object-cover h-75 w-full rounded-tl-md shadow-lg mx-auto"
                                     src={NuclearReactor}/>
                                <div className="h-25 w-full flex-1 flex rounded-bl-md bg-gray-800">
                                    <div className="text-left text-gray-100 font-extrabold w-75 py-1 px-2">
                                        <span className="text-l">Nuclear reactor</span>
                                        <br/>
                                        <span className="text-sm">Main energy source of the Battleship</span>
                                    </div>
                                    <div className="w-25 bg-gray-900 text-center items-center justify-items-center px-1">
                                        <br/>
                                        <span className="text-l">5</span>
                                    </div>

                                </div>
                            </button>
                        </div>

                        {/* BUILDING 2 */}
                        <div className="flex-1 h-72 bg-gradient-to-b from-gray-700 via-violet-900 to-pink-900 text-center p-2">
                            <button className="flex-none h-full w-full hover:opacity-25 transition duration-350 ease-in-out">
                                <img className="object-cover h-75 w-full shadow-lg mx-auto"
                                     src={NuclearReactor}/>
                                <div className="h-25 w-full flex-1 flex bg-gray-800">
                                    <div className="text-left text-gray-100 font-extrabold w-75 py-1 px-2">
                                        <span className="text-l">Nuclear reactor</span>
                                        <br/>
                                        <span className="text-sm">Main energy source of the Battleship</span>
                                    </div>
                                    <div className="w-25 bg-gray-900 text-center items-center justify-items-center px-1">
                                        <br/>
                                        <span className="text-l">5</span>
                                    </div>
                                </div>
                            </button>

                        </div>

                        {/* BUILDING 3 */}
                        <div className="flex-1 h-72 bg-gradient-to-bl from-gray-700 via-violet-900 to-pink-900 rounded-r-md text-center p-2">
                            <button className="flex-none h-full w-full hover:opacity-25 transition duration-350 ease-in-out">
                                <img className="object-cover h-75 w-full rounded-tl-md shadow-lg mx-auto"
                                     src={NuclearReactor}/>
                                <div className="h-25 w-full flex-1 flex rounded-bl-md bg-gray-800">
                                    <div className="text-left text-gray-100 font-extrabold w-75 py-1 px-2">
                                        <span className="text-l">Nuclear reactor</span>
                                        <br/>
                                        <span className="text-sm">Main energy source of the Battleship</span>
                                    </div>
                                    <div className="w-25 bg-gray-900 text-center items-center justify-items-center px-1">
                                        <br/>
                                        <span className="text-l">5</span>
                                    </div>
                                </div>
                            </button>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}