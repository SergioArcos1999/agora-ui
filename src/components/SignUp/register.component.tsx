import { Component } from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import AuthService from "../../services/auth.service";
import Image from "../../images/background-station-signup.jpg";
import {RouteComponentProps} from "react-router-dom";

interface RouterProps {
    history: string;
}
type Props = RouteComponentProps<RouterProps>;

type State = {
    username: string,
    email: string,
    password: string,
    successful: boolean,
    message: string
};

export default class Register extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.handleRegister = this.handleRegister.bind(this);

        this.state = {
            username: "",
            email: "",
            password: "",
            successful: false,
            message: ""
        };
    }

    validationSchema() {
        return Yup.object().shape({
            username: Yup.string()
                .test(
                    "len",
                    "The username must be between 3 and 20 characters.",
                    (val: any) =>
                        val &&
                        val.toString().length >= 3 &&
                        val.toString().length <= 20
                )
                .required("This field is required!"),
            email: Yup.string()
                .email("This is not a valid email.")
                .required("This field is required!"),
            password: Yup.string()
                .test(
                    "len",
                    "The password must be between 6 and 40 characters.",
                    (val: any) =>
                        val &&
                        val.toString().length >= 6 &&
                        val.toString().length <= 40
                )
                .required("This field is required!"),
        });
    }

    handleRegister(formValue: { username: string; email: string; password: string }) {
        const { username, email, password } = formValue;

        this.setState({
            message: "",
            successful: false
        });

        AuthService.register(
            username,
            email,
            password
        ).then(
            response => {
                this.setState({
                    message: response.data.message,
                    successful: true
                });
            },
            error => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                this.setState({
                    successful: false,
                    message: resMessage
                });
            }
        );
    }

    render() {
        const { successful, message } = this.state;

        const initialValues = {
            username: "",
            email: "",
            password: "",
        };

        return (
            <div className="h-full w-full flex place-content-stretch">
                <div className="w-100 mx-auto bg-gray-800 shadow-md max-w-sm p-4">

                    <Formik
                        className="space-y-6"
                        initialValues={initialValues}
                        validationSchema={this.validationSchema}
                        onSubmit={this.handleRegister}
                    >
                        <Form className="space-y-3">
                            {!successful && (
                                <div>
                                    {/* HEADER */}
                                    <span className="text-xl font-medium">Sign up</span>

                                    {/* USERNAME */}
                                    <div className="form-group">
                                        <label htmlFor="username"
                                               className="text-sm font-medium text-gray-100 block mb-2">Username</label>
                                        <Field name="username" type="text" className="form-control" placeholder="Username"/>
                                        <ErrorMessage name="username" component="div" className="alert alert-danger"/>
                                    </div>

                                    {/* EMAIL */}
                                    <div className="form-group">
                                        <label htmlFor="email"
                                               className="text-sm font-medium text-gray-100 block mb-2"> Email </label>
                                        <Field name="email" type="email" className="form-control" placeholder="email@example.com"/>
                                        <ErrorMessage name="email" component="div" className="alert alert-danger"/>
                                    </div>

                                    {/* PASSWORD */}
                                    <div className="form-group">
                                        <label htmlFor="password"
                                               className="text-sm font-medium text-gray-100 block mb-2"> Password </label>
                                        <Field name="password" type="password" className="form-control" placeholder="••••••••••"/>
                                        <ErrorMessage name="password" component="div" className="alert alert-danger"/>
                                    </div>

                                    <br/>

                                    {/* SUBMIT */}
                                    <div className="form-group">
                                        <button type="submit"
                                                className="w-full text-grey-100 bg-gradient-to-bl from-gray-700 via-violet-900 to-pink-900 hover:from-pink-900 hover:via-violet-900 hover:to-gray-700 focus:ring-1 focus:ring-violet-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center">
                                            Create account
                                        </button>
                                    </div>

                                    {/* ALREADY ACCOUNT */}
                                    <div className="text-sm font-medium text-gray-100 py-3">
                                        Already have an account? <a href="/sign-in" className="text-blue-400 hover:underline">Sign In</a>
                                    </div>
                                </div>
                            )}

                            {message && (
                                <div className="form-group">
                                    <div
                                        className={
                                            successful ? "alert alert-success" : "alert alert-danger"
                                        }
                                        role="alert"
                                    >
                                            {message}
                                    </div>
                                    {/* GO TO SIGN IN */}
                                    <div className="text-sm font-medium text-gray-100 py-3">
                                        Go to <a href="/sign-in" className="text-blue-400 hover:underline">Sign In</a>
                                    </div>
                                </div>
                            )}
                        </Form>
                    </Formik>
                </div>

                {/* BACKGROUND */}
                <div className="w-100 mx-auto shadow-md">
                    <img className="object-cover h-full w-full" src={Image}/>
                </div>
            </div>
        );
    }
}