import {Component} from "react";
import "./news.css";

import Ship from "../../images/ships/HomePageShip2.png";
import ComingSoon from "../../images/other/stellaris-federations.jpg";

type Props = {};

type State = {}

export default class News extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

    }

    render() {
        return (
            <div className="h-full w-full px-5 py-4">
                <div className="rounded-md shadow bg-gray-800 px-5 py-4 space-y-4">

                    <div className="text-3xl font-extrabold">
                        {/* TODO: Meterle un gradient al texto, por algún motivo no consigo sacarlo  https://tailwindcss.com/docs/background-clip#cropping-to-text*/}
                        <span
                            className="bg-gradient-to-r from-violet-900 to-violet-500 bg-clip-text text-transparent">NEWS</span>
                    </div>

                    <div className="flex space-x-4 justify-items-stretch">
                        <div className="flex-1 space-y-4">

                            <div className="flex-1 bg-gradient-to-br from-gray-700 via-violet-900 to-pink-900 rounded-md shadow p-3">

                                {/* COMING SOON */}
                                <h4>Mothership coming soon for browser!</h4>
                                <span className="text-sm">26-12-2021</span>
                                <br/><br/>
                                <div className="h-full w-full">
                                    <img className="object-cover h-full w-50 rounded-md shadow-lg mx-auto"
                                         src={ComingSoon}/>
                                </div>
                                <br/><br/>
                                <div className="px-8">
                                    <span>
                                    Welcome galactic leaders to the new space simulation game for the browser! I am still working on the design of the core Universe, but there will be news very soon!
                                    </span>
                                    <br/><br/>
                                    <span>
                                    Yours sincerely, the creator of the universe.
                                    </span>
                                    <br/>
                                    <span>
                                    S.A.
                                    </span>
                                </div>
                            </div>

                            {/* PATCH NOTES */}
                            <div className="bg-gradient-to-bl from-gray-700 via-violet-900 to-pink-900 rounded-md shadow p-3">
                                <h4>Patch notes v0.1 </h4>
                                <span className="text-sm">26-12-2021</span>
                                <br/><br/>

                                <h5>UI:</h5>
                                <ul role="list" className="list-disc marker:text-violet-800 space-y-1">
                                    <li>Major improvement in all non-game pages</li>
                                    <li>New "News" page</li>
                                    <li>New "Leaderboard" page</li>
                                    <li>New "Home" page</li>
                                    <li>New design and color palette</li>
                                </ul>

                                <h5>Gameplay:</h5>
                                <ul className="list-disc">
                                    <li>Still in progress...</li>
                                </ul>

                                <span>Do you have any recommendations or do you want to contribute to the project? Contact me: sergi.arcos@ielesvinyes.net</span>
                            </div>

                        </div>

                        <div className="flex-none w-1/6 bg-red-900 rounded-md shadow text-center p-3 bg-gradient-to-tl from-gray-700 via-violet-900 to-pink-900">
                            <div>PUBLICIDAD</div>
                        </div>
                    </div>
                </div>

                {/*<div className="home-page-animation h-96 w-96">
                        <img className="ship" src={Ship}/>
                    </div> */}
            </div>
        );
    }
}