import {Component} from "react";

import StatsService from "../../services/stats.service";
import Ship from "../../images/ships/HomePageShip2.png";
import FirstImage from "../../images/background-purple2.jpg";
import SecondImage from "../../images/background-battleship.jpg";
import ThirdImage from "../../images/background-courier-ship.jpg";

type Props = {};

type State = {
    totalPlayers: string;
    onlinePlayers: string;
    serverStatus: string;
}

export default class Leaderboard extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            totalPlayers: "",
            onlinePlayers: "",
            serverStatus: ""
        };
    }

    componentDidMount() {
        StatsService.getTotalNumberOfPlayers().then(
            response => {
                this.setState({
                    totalPlayers: response.data
                });
            },
            error => {
                this.setState({
                    totalPlayers: "243"
                });
            }
        );

        StatsService.getOnlinePlayers().then(
            response => {
                this.setState({
                    onlinePlayers: response.data
                });
            },
            error => {
                this.setState({
                    onlinePlayers: "23"
                });
            }
        );

        StatsService.getServerStatus().then(
            response => {
                this.setState({
                    serverStatus: response.status === 200 ? "ONLINE" : "OFFLINE"
                });
            },
            error => {
                this.setState({
                    serverStatus: "OFFLINE"
                });
            }
        );
    }

    render() {
        return (
            <div className="h-full w-full px-5 py-4">
                <div className="rounded-md shadow bg-gray-800 px-5 py-4 space-y-4">
                    <div className="text-3xl font-extrabold">
                        {/* TODO: Meterle un gradient al texto, por algún motivo no consigo sacarlo  https://tailwindcss.com/docs/background-clip#cropping-to-text*/}
                        <span className="bg-gradient-to-r from-violet-900 to-violet-700 bg-clip-text text-transparent">LEADERBOARD</span>
                    </div>
                    <br/>

                    {/* TOP 10 PLAYERS */}
                    <h3>Top 10 players</h3>
                    <div className="flex w-full">
                        <div className="w-full text-left">
                            <h4 className="text-white">Player</h4>
                        </div>
                        <div className="w-full text-center">
                            <h4 className="text-white">Score</h4>
                        </div>

                    </div>

                    <div className="grid grid-cols-2 space-y-1 place-items-left bg-gradient-to-br from-gray-700 via-violet-900 to-pink-900 rounded-md shadow p-3">
                        {/* HEADER */}
                        {/* 1 */}
                        <div>
                            <span className="text-white">1. Pussy Comander</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 2 */}
                        <div>
                            <span className="text-white">2. Dr.Pussy</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 3 */}
                        <div>
                            <span className="text-white">3. MacroPussy</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 4 */}
                        <div>
                            <span className="text-white">4. I'm not a pussy</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 5 */}
                        <div>
                            <span className="text-white">5. DigiCunt</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 6 */}
                        <div>
                            <span className="text-white">6. PussyMarauder</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 7 */}
                        <div>
                            <span className="text-white">7. Crussy</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 8 */}
                        <div>
                            <span className="text-white">8. Galactic Pussy</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 9 */}
                        <div>
                            <span className="text-white">9. Pussy Pussy</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>

                        {/* 10 */}
                        <div>
                            <span className="text-white">10. Susi Pussy</span>
                        </div>
                        <div className="text-center">
                            <span className="text-white">1.900.000</span>
                        </div>
                    </div>

                    <br/>
                    <h3>Other server stats</h3>

                    <div className="bg-gradient-to-bl from-gray-700 via-violet-900 to-pink-900 rounded-md shadow p-3">
                        <div>

                            <header>
                                <h5>TOTAL NUMBER OF PLAYERS:</h5>
                                {this.state.totalPlayers}
                            </header>
                            <br/>
                            <header>
                                <h5>PLAYERS ONLINE NOW:</h5>
                                {this.state.onlinePlayers}
                            </header>
                            <br/>
                            <header>
                                <h5>SERVER STATUS:</h5>
                                {this.state.serverStatus}
                            </header>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}