import {Component} from "react";

import StatsService from "../../services/stats.service";

type Props = {};

type State = {
    totalPlayers: string;
    onlinePlayers: string;
    serverStatus: string;
}

export default class Dashboard extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            totalPlayers: "",
            onlinePlayers: "",
            serverStatus: ""
        };
    }

    componentDidMount() {
        StatsService.getTotalNumberOfPlayers().then(
            response => {
                this.setState({
                    totalPlayers: response.data
                });
            },
            error => {
                this.setState({
                    totalPlayers: "243"
                });
            }
        );

        StatsService.getOnlinePlayers().then(
            response => {
                this.setState({
                    onlinePlayers: response.data
                });
            },
            error => {
                this.setState({
                    onlinePlayers: "23"
                });
            }
        );

        StatsService.getServerStatus().then(
            response => {
                this.setState({
                    serverStatus: response.status === 200 ? "ONLINE" : "OFFLINE"
                });
            },
            error => {
                this.setState({
                    serverStatus: "OFFLINE"
                });
            }
        );
    }

    render() {
        return (
            <div className="h-full w-full px-5 py-4">
                <div className="rounded-md shadow bg-gray-800 px-5 py-4 space-y-4">
                    <div className="text-3xl font-extrabold">
                        {/* TODO: Meterle un gradient al texto, por algún motivo no consigo sacarlo  https://tailwindcss.com/docs/background-clip#cropping-to-text*/}
                        <span className="bg-gradient-to-r from-violet-900 to-violet-700 bg-clip-text text-transparent">DASHBOARD</span>
                    </div>

                </div>

            </div>
        );
    }
}