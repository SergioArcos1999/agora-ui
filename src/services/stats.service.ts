import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/api/server-stats/';

class StatsService {
    getTotalNumberOfPlayers() {
        return axios.get(API_URL + "total-players", { headers: authHeader() })
    }

    getOnlinePlayers() {
        return axios.get(API_URL + "online-players", { headers: authHeader() })
    }

    getServerStatus() {
        return axios.get(API_URL + "server-status", {headers: authHeader()})
    }
}

export default new StatsService();
